<?php
/**
 * @class  VMJsonServiceModule
 * Description of VMJsonServiceModule class
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
Yii::setPathOfAlias('json-service', __DIR__ . DIRECTORY_SEPARATOR);

class VMJsonServiceModule extends CWebModule {
	public function init() {

		$this->setImport(array(
			'json-service.components.*',
			'json-service.components.widgets.*',
			'json-service.controllers.*',
		));

		if (YII_DEBUG) {
			$this->controllerMap = array(
				'documentation' => array(
					'class' => 'VMDocumentationController'
				)
			);
		}
	}
} 