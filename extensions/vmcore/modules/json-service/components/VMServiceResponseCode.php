<?php

class VMServiceResponseCode
{
	const NO_ERROR = 200;
	const SERVICE_ERROR = -1;
	const THIRD_SERVICE_ERROR = -2;
}
