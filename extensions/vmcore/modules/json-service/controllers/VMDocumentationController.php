<?php

/**
 * @class  VMDocumentationController
 * Description of VMDocumentationController class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */

class VMDocumentationController extends CController
{
	/**
	 * @param string $controller
	 */
	public function actionIndex($controller = '') {
		$controller = ucfirst($controller);
		Yii::import('yiivm.extensions.metadata.Metadata');

		$metadata = new Metadata;

		$controllers = VMObjectUtils::fromArray($metadata->getControllersActions(Yii::app()->controller->module->id));

		$definitions = array();

		if($controller) {
			$className = $controller . 'Controller';

			$reflectionClass = new ReflectionClass($className);

			$controllerClass = new $className($controller, Yii::app()->controller->module->id);
			$controllerClass->documentationMode = true;

			$actions = $metadata->getActions($className, Yii::app()->controller->module->id);

			foreach($actions as $action) {
				$reflectionMethod = $reflectionClass->getMethod('action' . $action);

				$definition = $this->getDefinition($controllerClass, $reflectionMethod->name);

				$doc = $this->cleanDocumentation($reflectionMethod->getDocComment());

				if ($definition) {
					$definitions[] = (object)array(
						'name' => $action,
						'description' => $doc,
						'method' => sprintf('%s/%s/%s', Yii::app()->controller->module->id, lcfirst($controller), lcfirst($action)),
						'parameters' => $definition->parameters
					);
				}
			}
		}

		$this->render('json-service.views.documentation.index', array(
			'controllers' => $controllers,
			'definitions' => $definitions,
			'controller' => $controller,
		));
	}

	/**
	 * @param $class
	 * @param $method
	 *
	 * @return object
	 */
	private function getDefinition($class, $method)
	{
		try {
			$class->{$method}();
		} catch (VMDocumentationException $e) {
			// That is planned for it
			return (object) array(
				'parameters' => $e->parameters
			);
		}
	}

	/**
	 * @param $rawDoc
	 *
	 * @return null|string
	 */
	private function cleanDocumentation($rawDoc)
	{
		$comment = null;

		if (preg_match('#^/\*\*(.*)\*/#s', $rawDoc, $comment) === false) {
			return null;
		};

		if (count($comment) < 2) {
			return null;
		}

		// I have no ideas why
		$comment = trim($comment[1]);

		//Get all the lines and strip the * from the first character
		if (preg_match_all('#^\s*\*(.*)#m', $comment, $lines) === false) {
			return null;
		}

		return implode(PHP_EOL, $lines[1]);
	}
}