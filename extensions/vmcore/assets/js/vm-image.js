/**
 * Created with JetBrains PhpStorm.
 * User: egor
 * Date: 04.12.13
 * Time: 13:21
 * To change this template use File | Settings | File Templates.
 */
/* jsHint browser:true*/
/* global window:true */
(function ($, Error, undefined) {
    'use strict';

    if (!$.isString) {
        $.isString = function (expression) {
            return typeof expression === $.isString.validType;
        };
        $.isString.validType = typeof '';
    }

    /**
     * @class VMImage
     * @constructor
     * @this {VMImage}
     * @param {jQuery} jQ
     * @param {Object} options
     * */
    function VMImage(jQ, options) {
        jQ.parent().css({overflow: 'hidden', display: 'block'});
        jQ.css({maxWidth: 'none', minWidth: '0px', maxHeight: 'none', minHeight: '0px'});
        this.$ = jQ;
        this.options = $.extend({}, this.$default.options, options);
    }

    var prototype = VMImage.prototype;

    prototype.$default = {
        options: {
        },
        keys: {
            customWidth: 'custom-width',
            customHeight: 'custom-height',
            realHeight: 'real-height',
            realWidth: 'real-width',
            text: 'text'
        },
        animation: {
            duration: 400
        }
    };

    prototype.crop = function (options) {
        options = $.extend({}, this.options, options);
        if (this.$.get(0).naturalHeight && this.$.get(0).naturalWidth && this.$.get(0).complete) {
            return this.$crop(options);
        }
        return this.$cropAfterLoad(options);
    };

    prototype.$crop = function (options) {
        var cssOptions = this.$calculateCssOptions(options);

        var imgStyle = $.extend({}, cssOptions, {opacity: 1});

        var wrapperStyle = {
            height: options.customHeight,
            width: options.customWidth
        };

        this.$.parent().css(wrapperStyle);

        if (options.withAnimation) {
            options.withAnimation = $.extend({}, this.$default.animation, options.withAnimation);
            var complete = this.options.complete;
            options.withAnimation.complete = function () {
                $(this).trigger($.Event('vmImage.crop.complete'));
                if ($.isFunction(complete)) {
                    complete.call($(this));
                }
            };
            this.$.animate(imgStyle, options.withAnimation);
        } else {
            this.$.css(imgStyle);
            this.$.trigger($.Event('vmImage.crop.complete'));
        }
    };

    prototype.$cropAfterLoad = function (options) {
        if (this.$.get(0).complete) {
            this.$onFailLoad(this.$, options);
            return;
        }
        this.$.css({opacity: 0});
        this.$
            .one('load', function () {
                $(this).data($.fn.vmImage.dataKey).$crop(options);
            })
            .one('error', VMImage.prototype.$onFailLoad.bind(this, this.$, options));
    };

    prototype.$onFailLoad = function (img, options) {
        options = this.$getCustomSize(options);
        img.off('load');
        var wrapperStyle = {
            height: options.height,
            width: options.width
        };
        img.parent().css(wrapperStyle);
        img.css({opacity: 1});
        img.attr('src', 'http://placehold.it/' + options.width + 'x' + options.height + '&text=' + this.options.text);
    };

    prototype.$getRealSize = function () {
        var width = parseInt(this.$.get(0).width || this.$.data(this.$default.keys.realWidth), 10);
        var height = parseInt(this.$.get(0).height || this.$.data(this.$default.keys.realHeight), 10);
        this.$.data(this.$default.keys.realWidth, width);
        this.$.data(this.$default.keys.realHeight, height);
        return {
            width: width,
            height: height
        };
    };

    prototype.$getCustomSize = function (options) {
        var width = parseInt(options.customWidth || this.$.data(this.$default.keys.customWidth), 10);
        var height = parseInt(options.customHeight || this.$.data(this.$default.keys.customHeight), 10);
        this.$.data(this.$default.keys.customWidth, width);
        this.$.data(this.$default.keys.customHeight, height);
        return {
            width: width,
            height: height
        };
    };

    prototype.$calculateCssOptions = function (options) {
        var size = {
            real: this.$getRealSize(),
            custom: this.$getCustomSize(options)
        };
        size.align = {};
        size.align.height = size.custom.width * (size.real.height / size.real.width);
        size.align.width = size.custom.height * (size.real.width / size.real.height);
        size.difference = {};
        size.difference.height = size.align.height - size.custom.height;
        size.difference.width = size.align.width - size.custom.width;

        var cssOptions;
        var isSmall = size.real.width < size.custom.width && size.real.height < size.custom.height;
        var isBig = size.real.width > size.custom.width && size.real.height > size.custom.height;

        if (isSmall) {
            cssOptions = this.$resizeSmallToBig(size);
        } else if (isBig) {
            cssOptions = this.$resizeBigToSmall(size);
        } else if (size.difference.height > size.difference.width) {
            cssOptions = this.$alignWidth(size);
        } else {
            cssOptions = this.$alignHeight(size);
        }

        cssOptions.marginLeft = ((size.custom.width - cssOptions.width) / 2) + 'px';
        cssOptions.marginTop = ((size.custom.height - cssOptions.height) / 2) + 'px';
        cssOptions.width = cssOptions.width + 'px';
        cssOptions.height = cssOptions.height + 'px';
        return cssOptions;
    };

    prototype.$resizeSmallToBig = function (size) {
        if (size.difference.height < size.difference.width) {
            return this.$alignHeight(size);
        }
        return this.$alignWidth(size);
    };

    prototype.$resizeBigToSmall = function (size) {
        if (size.difference.width > size.difference.height) {
            return this.$alignHeight(size);
        }
        return this.$alignWidth(size);
    };

    prototype.$alignHeight = function (size) {
        return{
            height: size.custom.height,
            width: size.align.width
        };
    };

    prototype.$alignWidth = function (size) {
        return{
            height: size.align.height,
            width: size.custom.width
        };
    };

    $.fn.vmImage = function (options) {
        if (this.length === 0) {
            return this;
        }

        if (this.length > 1) {
            options = Array.prototype.slice.call(arguments);
            return this.each(function () {
                $.fn.vmImage.apply($(this), options);
            });
        }

        if (this.get(0).tagName !== 'IMG') {
            throw new Error('this is not image');
        }

        var command;
        if ($.isString(options)) {
            command = options;
            options = Array.prototype.slice.call(arguments, 1);
        }

        var vmImage = this.data($.fn.vmImage.dataKey);

        if (vmImage === undefined) {
            vmImage = new VMImage(this, command ? options[0] : options);
            this.each(function () {
                $.data(this, $.fn.vmImage.dataKey, vmImage);
            });
        }

        if (command !== undefined) {
            command = command.replace('$', '');
            if ($.isFunction(vmImage[command])) {
                return vmImage[command].apply(vmImage, options);
            }
        }

        return this;
    };

    $.fn.vmImage.dataKey = 'vmImage';
})(window.jQuery, window.Error);