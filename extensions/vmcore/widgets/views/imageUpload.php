<?php
	/**
	 * @var CActiveRecord $model
	 */
?>
<div class="widget-crop-upload-image"
     data-upload-url="<?=$uploadUrl?>"
     data-remove-url="<?=$removeUrl?>"
     data-submit-with-form="<?=$submitWithForm?>"
     data-send-on-change="<?=$sendOnChange?>"
     data-custom-height="<?=$height?>"
     data-custom-width = "<?=$width?>"
     data-text="<?=$text?>"
>

	<div class="widget-crop-image">
		<?php
			echo CHtml::image(Yii::app()->createUrl($model->{$attribute}), $model->getAttributeLabel($attribute));
		?>
	</div>

    <span class="widget-wrap-upload">
	    <span class="widget-button-file"><i class="icon-folder-open"></i></span>
        <?
        echo CHtml::fileField(CHtml::activeName($model, $attribute), '', array(
            'class' => 'widget-input-file',
            'id' => CHtml::activeId($model, $attribute)
        ));
        ?>
	</span>

    <span class="widget-button-close" title="remove"><i class="icon-remove"></i></span>
</div>