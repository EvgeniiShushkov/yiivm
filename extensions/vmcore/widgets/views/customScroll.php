<?php
/**
 * @var $content
 * @var $class
 * @var $options
 */
?>

<div class="vm-scroller-wrapper<?php echo $class; ?>"<?php echo $options; ?>>
	<div class="vm-scroller">
		<div class="vm-scroller-container">
			<?php echo $content; ?>
		</div>
		<div class="vm-scroller-bar"></div>
	</div>
</div>