<?php

class VMUpload extends CApplicationComponent {
	public $targetFolder     = 'media';
	public $lastUploadedFile = null;

	/**
	 * @var CModel $model
	 */
	private $entity;
	private $attribute;

	/**
	 * @var CUploadedFile|string $data
	 */
	private $data;
	private $extension;

	public static function createFromModel (CModel $entity, $attribute) {
		$upload = new VMUpload($entity, $attribute);

		if (self::entityHasOwnProperty($entity, $attribute)) {
			$uploaded = CUploadedFile::getInstance($entity, $attribute);
		} else {
			$uploaded = CUploadedFile::getInstanceByName($attribute);
		}

		$upload->data = $uploaded;

		return $upload;
	}

	public static function createFromUrl (CModel $entity, $attribute, $url, $extension) {
		$upload = new VMUpload($entity, $attribute);

		$upload->data = file_get_contents($url);

		if(!$upload->data) {
			throw new CHttpException(404, Yii::t('vmcore.media', 'File not found'));
		}

		$upload->extension = $extension;

		return $upload;
	}

	public static function createFromBase64 (CModel $entity, $attribute, $base64data, $extension) {
		$upload = new VMUpload($entity, $attribute);

		$upload->data = base64_decode($base64data, true);

		if (!$upload->data) {
			throw new CHttpException(404, Yii::t('vmcore.media', 'Could not decode file'));
		}

		$upload->extension = $extension;

		return $upload;
	}

	public function save($uploadDir = null, $baseName = null) {
		$this->prepareSaving($this->entity, $uploadDir);

		if(VMObjectUtils::checkClass($this->data, 'CUploadedFile', false)) {
			$this->lastUploadedFile = $this->generateFilename($this->entity, $this->attribute, $baseName, $this->data->extensionName);

			if (!$this->data->saveAs($this->getFullPathOf($this->lastUploadedFile))) {
				throw new CException(Yii::t('vmcore.media', 'Could not save file'));
			}

			return $this->lastUploadedFile;
		}

		$this->lastUploadedFile = $this->generateFilename($this->entity, $this->attribute, $baseName, $this->extension);

		if (!file_put_contents($this->getFullPathOf($this->lastUploadedFile), $this->data)) {
			throw new CException(Yii::t('vmcore.media', 'Could not save file'));
		}

		return $this->lastUploadedFile;
	}

	public static function remove($mixed, $attribute) {
		$upload = new VMUpload();
		return $upload->quickRemove($mixed, $attribute);
	}

	public function __construct ($entity = null, $attribute = null) {
		$this->entity    = $entity;
		$this->attribute = $attribute;
	}

	protected function prepareSaving(CModel $entity, $path) {
		if (!$path) {
			VMObjectUtils::checkClass($entity, 'CActiveRecord');

			/** @noinspection PhpParamsInspection */
			$path = $this->getUploadDirectoryOfEntity($entity);
		}

		$this->createDirectory($path);
	}

	protected function generateFilename($entity, $attribute, $basename = null, $extension = null) {
		if ($basename) {
			$filename = $this->getRelativePathOfBasename($basename, $extension);
		} else {
			VMObjectUtils::checkClass($entity, 'CActiveRecord');

			/** @noinspection PhpParamsInspection */
			$filename = $this->getRelativePathOfEntity($entity, $attribute, $extension);
		}

		return $filename;
	}

	/**
	 * @param CModel $entity
	 * @param        $attribute
	 * @param null   $uploadDir
	 * @param null   $basename
	 *
	 * @return null|string
	 * @throws CException
	 *
	 * @deprecated
	 */
	public function quickSave(CModel $entity, $attribute, $uploadDir = null, $basename = null)
	{
		$this->prepareSaving($entity, $uploadDir);

		if (VMUpload::entityHasOwnProperty($entity, $attribute)) {
			$uploaded = CUploadedFile::getInstance($entity, $attribute);
		} else {
			$uploaded = CUploadedFile::getInstanceByName($attribute);
		}

		if ($uploaded) {
			$this->lastUploadedFile = $this->generateFilename($entity, $attribute, $basename, $uploaded->extensionName);

			if (!$uploaded->saveAs($this->getFullPathOf($this->lastUploadedFile))) {
				throw new CException(Yii::t('vmcore.media', 'Could not save file'));
			}

			return $this->lastUploadedFile;
		} else {
			return null;
		}
	}

	/**
	 * @param CModel $entity
	 * @param        $attribute
	 * @param        $url
	 * @param null   $extension
	 * @param null   $uploadDir
	 * @param null   $basename
	 *
	 * @return null|string
	 * @throws CException
	 *
	 * @deprecated
	 */
	public function urlQuickSave(CModel $entity, $attribute, $url, $extension = null, $uploadDir = null, $basename = null)
	{
		$this->prepareSaving($entity, $uploadDir);

		$uploaded = file_get_contents($url);

		if ($uploaded) {
			$this->lastUploadedFile = $this->generateFilename($entity, $attribute, $basename, $extension);

			if (!file_put_contents($this->getFullPathOf($this->lastUploadedFile), $uploaded)) {
				throw new CException(Yii::t('vmcore.media', 'Could not save file'));
			}

			return $this->lastUploadedFile;
		} else {
			return null;
		}
	}

	/**
	 * @param CModel $entity
	 * @param        $attribute
	 * @param        $extension
	 * @param null   $uploadDir
	 * @param null   $basename
	 *
	 * @return null|string
	 * @throws CException
	 *
	 * @deprecated
	 */
	public function base64quickSave(CModel $entity, $attribute, $extension, $uploadDir = null, $basename = null)
	{
		$this->prepareSaving($entity, $uploadDir);

		$uploaded = base64_decode($entity->{$attribute}, true);

		if ($uploaded) {
			$this->lastUploadedFile = $this->generateFilename($entity, $attribute, $basename, $extension);

			if (!file_put_contents($this->getFullPathOf($this->lastUploadedFile), $uploaded)) {
				throw new CException(Yii::t('vmcore.media', 'Could not save file'));
			}

			return $this->lastUploadedFile;
		} else {
			return null;
		}
	}

	public function getUploadDirectoryOfEntity(CActiveRecord $entity)
	{
		return $this->getUploadDirectory() . DIRECTORY_SEPARATOR . $entity->tableName();
	}

	public function getUploadDirectory()
	{
		return sprintf('%s/../%s', Yii::app()->basePath, $this->targetFolder);
	}

	protected function createDirectory($directory)
	{
		if (!file_exists($directory)) {
			if(!mkdir($directory, 0777, true)) {
				throw new CException(Yii::t('vmcore.media', 'Directory "{directory}" is not writable.', array('{directory}' => $directory)));
			}
		}
	}

	protected static function entityHasOwnProperty(CModel $entity, $prop)
	{
		return $entity->hasProperty($prop) || isset($entity[$prop]) || property_exists($entity, $prop);
	}

	public function getRelativePathOfBasename($basename, $extension)
	{
		if ($extension) {
			$basename .= '.' . $extension;
		}

		return $basename;
	}

	public function getRelativePathOfEntity(CActiveRecord $entity, $attribute, $extension = null)
	{
		if (is_array($entity->primaryKey)) {
			$key = implode('-', $entity->primaryKey);
		} else {
			$key = $entity->primaryKey;
		}

		$basename = $this->targetFolder . DIRECTORY_SEPARATOR . $entity->tableName() . DIRECTORY_SEPARATOR . $key . '-' . $attribute;
		return $this->getRelativePathOfBasename($basename, $extension);
	}

	public function getFullPathOf($filename)
	{
		return Yii::app()->basePath . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . $filename;
	}

	public function quickRemove($mixed, $attribute)
	{
		if(!$this->havePermission($mixed, $attribute)) {
			return false;
		}

		$result = true;

		if (is_array($mixed)) {
			foreach ($mixed as $model) {
				$result = $result && $this->quickRemoveFile($model, $attribute);
			}
		} else {
			$result = $this->quickRemoveFile($mixed, $attribute);
		}

		return $result;
	}

	protected function havePermission($mixed, $attribute)
	{
		if (is_array($mixed)) {
			foreach ($mixed as $model) {
				$path = $this->getFullPathOf($model->{$attribute});
				if (!is_writable($path)) {
					return false;
				}
			}
		} else {
			$path = $this->getFullPathOf($mixed->{$attribute});
			if (!is_writable($path)) {
				return false;
			}
		}

		return true;
	}

	protected function quickRemoveFile($model, $attribute)
	{
		try {
			if (!$model->{$attribute}) {
				return false;
			}

			$path = $this->getFullPathOf($model->{$attribute});
			if (is_writable($path)) {
				unlink($path);
			}
		} catch (Exception $exception) {
			return false;
		}

		return true;
	}
}