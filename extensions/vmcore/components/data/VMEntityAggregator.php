<?php

class VMEntityAggregator extends CComponent
{
	private $models = array();

	public function __construct($mixed)
	{
		if (is_a($mixed, 'CActiveDataProvider')) {

			$this->loadActiveDataProvider($mixed);
		} else if (is_array($mixed)) {
			$this->models = $mixed;
		}
	}

	private function loadActiveDataProvider($dataProvider)
	{
		$this->models = CActiveRecord::model($dataProvider->modelClass)->findAll($dataProvider->criteria);
	}

	public function count($filter = null)
	{
		$count = 0;

		foreach ($this->models as $model) {
			if (($filter && call_user_func($filter, $model)) || !$filter) {
				$count++;
			}
		}

		return $count;
	}

	public function sum($column, $filter = null)
	{
		$total = 0;

		foreach ($this->models as $model) {
			if (($filter && call_user_func($filter, $model)) || !$filter) {
				$total += $model->attributes[$column];
			}
		}

		return $total;
	}

	public function values($column, $filter = null, $distinct = false)
	{
		$result = array();

		foreach ($this->models as $model) {
			if (!$filter || ($filter && call_user_func($filter, $model))) {
				$value = $model->attributes[$column];
				if ($value) {
					if (!$distinct || ($distinct && !in_array($value, $result)))
						$result[] = $model->attributes[$column];
				}
			}
		}

		return $result;
	}
}
