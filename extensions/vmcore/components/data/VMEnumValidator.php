<?php
/**
 * @class VMEnumValidator
 * @example array('attribute', 'VMEnumValidator', 'range' => VMEnum::getConstantsMap)
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMEnumValidator extends CValidator {
	public $range;
	public $strict = null;

	/**
	 * Validates a VMEnum data
	 *
	 * @param CModel $object    the data object being validated
	 * @param string $attribute the name of the attribute to be validated.
	 *
	 * @throws CException if given {@link range} is not an array
	 */
	protected function validateAttribute($object, $attribute)
	{
		if (!is_array($this->range)) {
			throw new CException(Yii::t('vmcore.data', 'The "range" property must be specified with a list of values.'));
		}

		$values = $object->{$attribute};
		$result = true;

		if(is_array($values)) {
			foreach($values as $value) {
				$result = $result && in_array($value, $this->range, $this->strict);
			}
		} else {
			$result = in_array($values, $this->range, $this->strict);
		}

		if (!$result) {
			$this->addError($object, $attribute, Yii::t('vmcore.errors', '{property} is not set up properly', array('{property}' => $attribute)));
		}
	}
}