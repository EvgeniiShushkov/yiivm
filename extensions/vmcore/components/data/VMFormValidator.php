<?php

/**
 * @class  VMFormValidator
 * Description of VMFormValidator class
 *
 * @property CFormModel $form
 */
class VMFormValidator extends CComponent
{
	private $formClass;
	private $form;

	public function __construct($formClass)
	{
		if (!$formClass) {
			throw new CException(Yii::t('vmcore.errors', '{property} is not set up properly', array('{property}' => 'formClass')));
		}

		$this->formClass = $formClass;
		$this->form      = new $formClass();
	}

	public function validate($scenario = null, $ajaxValidate = false)
	{
		$this->form->setScenario($scenario);
		$attributes = Yii::app()->request->getParam($this->formClass);

		$result = false;

		if ($attributes) {
			$this->form->attributes = $attributes;

			if($ajaxValidate) {
				$errors = CActiveForm::validate($this->form);

				echo $errors;

				if($errors == CJavaScript::encode(array())) {
					$result = true;
				}
			} else {
				$result = $this->form->validate();
			}
		}

		return $result;
	}

	public function getForm()
	{
		return $this->form;
	}
}