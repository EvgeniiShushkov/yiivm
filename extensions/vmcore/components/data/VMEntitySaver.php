<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Class VMEntitySaver
 */
class VMEntitySaver extends CComponent {
	/**
	 * @var CActiveRecord
	 */
	public $entity = null;
	/**
	 * @var callable
	 */
	public $onBeforeSave = null;
	/**
	 * @var callable
	 */
	public $onAfterSave = null;
	/**
	 * @var callable
	 */
	public $savingResult = null;

	/**
	 * @param string $className
	 * @param array  $defaultParams
	 */
	public function __construct($className, $defaultParams = array()) {
		if (empty($defaultParams) && !(self::containsEntity($className))) {
			throw new CHttpException(400, Yii::t('vmcore.errors', 'There are no parameters matching "{className}"', array(
				'{className}' => $className
			)));
		}

		$params = !empty($defaultParams) ? (array)$defaultParams : (array)Yii::app()->request->getParam($className);

		$model = CActiveRecord::model($className);

		$primaryKey = $this->getPrimaryKey($model, $params);

		if (empty($primaryKey)) {
			$this->entity = new $className();
		} else {
			$this->entity = $model->findByPk($primaryKey);
		}

		foreach ($params as $attribute => $value) {
			if (($attribute !== $model->tableSchema->primaryKey)) {
				$this->entity[$attribute] = $value;
			}
		}
	}

	private function getPrimaryKey($model, $params) {
		$primaryKeys = $model->tableSchema->primaryKey;
		$keys        = array();
		if (is_array($primaryKeys)) {
			foreach ($primaryKeys as $keyName) {
				if (array_key_exists($keyName, $params)) {
					$keys = $params[$keyName];
				}
			}
		} elseif (array_key_exists($primaryKeys, $params)) {
			$keys = $params[$primaryKeys];
		}

		return $keys;
	}

	public static function containsEntity($className) {
		return Yii::app()->request->getParam($className) != null;
	}

	/**
	 * @return bool
	 * @throws Exception
	 */
	public function save() {
		if ($this->onBeforeSave) {
			call_user_func($this->onBeforeSave, new CEvent($this));
		}

		if (!$this->entity) {
			throw new CException(Yii::t('vmcore.data', 'Entity not set up'));
		}

		if ($this->entity->hasErrors()) {
			return false;
		}
		$transaction = new VMTransaction();

		try {
			$this->savingResult = $this->entity->save() && $this->entity->refresh();

			if ($this->onAfterSave) {
				call_user_func($this->onAfterSave, new CEvent($this, array('succeeded' => $this->savingResult)));
			}

			if ($this->savingResult) {
				$transaction->commit();
			}

			return $this->savingResult;
		} catch (Exception $e) {
			$transaction->rollback();
			throw $e;
		}
	}
}

