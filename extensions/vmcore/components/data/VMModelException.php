<?php

class VMModelException extends CException {
	public $entity = null;

	public function __construct(CModel $entity) {
		$messages     = array();
		$this->entity = $entity;

		foreach ($entity->errors as $attribute => $errors) {
			$messages[] = implode(' : ', array(
				'attribute' => $attribute,
				'message'   => implode(', ', (array)$errors)
			));
		}

		parent::__construct(implode(PHP_EOL, $messages));
	}
}