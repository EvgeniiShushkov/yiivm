<?php

return array(
    '{className} not found' => '{className} не найден',
    'There are no parameters matching "{className}"' => 'Нет параметров соответствующих {className}',
);