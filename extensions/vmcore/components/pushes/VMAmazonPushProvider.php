<?php
use Aws\CloudFront\Exception\Exception;
use Aws\Common\Aws;
use Aws\S3\S3Client;
use Aws\Sns\SnsClient;

spl_autoload_unregister(array('YiiBase', 'autoload'));
require_once __DIR__ . '/vendors/Aws/Autoloader.php';
spl_autoload_register(array('YiiBase', 'autoload'));

/**
 * @class  VMAmazonPushProvider
 * Description of VMAmazonPushProvider class
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMAmazonPushProvider extends VMBasePushProvider {
	public $region;
	public $application;
	public $client;

	private $target;

	public function __construct($config = array(), $className = __CLASS__) {
		parent::__construct($config, $className);

		$this->client = SnsClient::factory(array(
			'key'    => $this->applicationKey,
			'secret' => $this->apiKey,
			'region' => $this->region,
		));
	}

	private function send($alert, $data, $badge) {
		if (!$this->target) {
			$this->setResponse(self::STATUS_BAD_PARAM, Yii::t('vmcore.pushes', 'You must specify a target for sending'));

			return FALSE;
		}

		if (!$alert) {
			$this->setResponse(self::STATUS_BAD_PARAM, Yii::t('vmcore.pushes', 'You must specify a message for sending'));

			return FALSE;
		}

		try {
			$this->client->publish(array(
				'MessageStructure' => 'json',
				'Message'          => CJSON::encode(array(
					'default' => $alert,
					'APNS'    => CJSON::encode(array(
						'aps'  => array(
							'alert' => $alert,
							'badge' => $badge,
							'sound' => 'default',
						),
						// Custom payload parameters can go here
						'data' => $data
					))
				)),
				'TargetArn'        => $this->target
			));

			$this->setResponse(self::STATUS_OK, Yii::t('vmcore.pushes', 'Notification has been successfully sent'));

			return TRUE;
		} catch (Exception $e) {
			$this->setResponse($e->getCode(), $e->getMessage());

			return FALSE;
		}
	}

	/**
	 * @param string $alert
	 * @param null   $data
	 *
	 * @return bool
	 */
	public function sendPush($alert, $data = NULL) {
		return $this->send($alert, $data, NULL);
	}

	/**
	 * @param $alert
	 * @param $badge
	 *
	 * @return bool
	 */
	public function sendPushWithBadge($alert, $badge) {
		return $this->send($alert, NULL, $badge);
	}

	/**
	 * @param $alert
	 * @param $payload
	 * @param $badge
	 *
	 * @return bool
	 */
	public function sendPushWithBadgeAndPayload($alert, $payload, $badge){
		return $this->send($alert, $payload, $badge);

	}

	public function sendRichPush(VMPushModel $model) {
		if (!$this->target) {
			$this->setResponse(self::STATUS_BAD_PARAM, Yii::t('vmcore.pushes', 'You must specify a target for sending'));

			return FALSE;
		}

		if (!$model->alert) {
			$this->setResponse(self::STATUS_BAD_PARAM, Yii::t('vmcore.pushes', 'You must specify a message for sending'));

			return FALSE;
		}

		try {
			$this->client->publish(array(
				'MessageStructure' => 'json',
				'Message'          => CJSON::encode(array(
					'default' => $model->alert,
					'APNS'    => CJSON::encode(array(
						'aps'  => array(
							'alert' => $model->alert,
							'badge' => $model->badge,
							'sound' => $model->sound
						),
						// Custom payload parameters can go here
						'data' => $model->data
					))
				)),
				'TargetArn'        => $this->target
			));

			$this->setResponse(self::STATUS_OK, Yii::t('vmcore.pushes', 'Notification has been successfully sent'));

			return TRUE;
		} catch (Exception $e) {
			$this->setResponse($e->getCode(), $e->getMessage());

			return FALSE;
		}
	}

	public function registerTarget($deviceToken) {
		$response = $this->client->createPlatformEndpoint(array(
			'PlatformApplicationArn' => $this->application,
			'Token'                  => $deviceToken,
		));

		return $response['EndpointArn'];
	}

	public function setTarget($target) {
		$this->target = $target;
	}
}