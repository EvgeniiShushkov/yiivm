<?php
/**
 * Created by JetBrains PhpStorm.
 * User: egor
 * Date: 21.02.14
 * Time: 19:09
 * To change this template use File | Settings | File Templates.
 */
/**
 * @class VMPushModel
 * @property string $badge
 * @property string $alert
 * @property string $sound
 * @property object $data
 */
class VMPushModel
{

    public $badge;
    public $alert;
    public $sound;
    public $data;

}