<?php
/**
 * @class VMUtcWidget
 * Description of VMUtcWidget class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMUtcWidget extends CWidget
{
    public $referrer = false;

    public function run()
    {
        parent::run();
        /* @var $cs CClientScript */
        $cs = Yii::app()->clientScript;
        $cs->registerCoreScript('jquery');

        $this->render('datetime', array(
            'referrer' => $this->referrer
        ));
    }
} 